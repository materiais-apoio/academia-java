import java.time.Duration;
import java.time.LocalDate;

/**
 * Essa era uma questão para se procurar métodos na documentação Java.
 * Mas é possível fazer o algoritmo correto "na mão".
 */
public class AritmeticaDatas {
    private static long calcularDuracaoHoras(LocalDate inicio, LocalDate fim) {
        return Duration.from(inicio.until(fim)).toHours();
    }
}
