/**
 * Questão 1. INMETRO.
 */
public enum ConsumoCpu {
    CONCEITO_A("A", 5.0f),
    CONCEITO_B("B", 10.0f),
    CONCEITO_C("C", 25.0f),
    CONCEITO_D("D", 65.0f),
    CONCEITO_E("E", Float.POSITIVE_INFINITY);

    public static ConsumoCpu definirConceito(float tpu) {
        if (tpu < CONCEITO_A.limSuperior) {
            return CONCEITO_A;
        } else if (tpu < CONCEITO_B.limSuperior) {
            return CONCEITO_B;
        } else if (tpu < CONCEITO_C.limSuperior) {
            return CONCEITO_C;
        } else if (tpu < CONCEITO_D.limSuperior) {
            return CONCEITO_D;
        } else {
            return CONCEITO_E;
        }
    }

    public static ConsumoCpu definirConceitoMelhor(float tpu) {
        ConsumoCpu[] conceitos = ConsumoCpu.values();

        for (ConsumoCpu conceito : conceitos) {
            if (tpu < conceito.limSuperior) {
                return conceito;
            }
        }
        return CONCEITO_E; // Nunca vai acontecer, mas o compilador não sabe
    }

    public String getNome() {
        return nome;
    }


    private ConsumoCpu(String nome, float limSuperior) {
        this.nome = nome;
        this.limSuperior = limSuperior;
    }

    private final String nome;
    private final float  limSuperior;
}