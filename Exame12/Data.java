import java.time.DateTimeException;
import java.util.List;
import java.util.Optional;

public class Data {
    
    public static final Data diaUm = new Data(01,01,1970);
    
    public static Data criarData(int dia, int mes, int ano) {
        if (mes < 1 || mes > 12 || dia < 1 || dia > 31) {
            throw new DateTimeException("");
        }

        if (mes == 2 && dia > 28) {
            throw new DateTimeException("");
        }

        switch (mes) {
            case 4: case 6: case 9: case 11: 
                if (dia > 30) {
                    throw new DateTimeException("");
                }
                break;

            default:
        }

        return new Data(dia, mes, ano);
    }

    public static Optional<Data> criarDataMelhor(int dia, int mes, int ano) {
        if (mes < 1 || mes > 12 || dia < 1 || dia > 31) {
            return Optional.empty();
        }

        if (mes == 2 && dia > 28) {
            return Optional.empty();
        }

        if (List.of(4,6,9,11).contains(mes) && dia > 30) {
            return Optional.empty();
        }

        return Optional.of(new Data(dia, mes, ano));
    }

    public Data(int dia, int mes, int ano) {
        this.dia = dia;
        this.mes = mes;
        this.ano = ano;
    }

    public int getDia() {
        return dia;
    }

    public int getMes() {
        return mes;
    }

    public int getAno() {
        return ano;
    }

    public String mostrarData() {
        return dia +"/"+ mes +"/"+ ano;
    }

    private int dia;
    private int mes;
    private int ano;
}
