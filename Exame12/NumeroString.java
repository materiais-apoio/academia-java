import java.util.Optional;
import java.util.function.Predicate;
import java.util.regex.Pattern;

public class NumeroString {
    private static Predicate<String> validadorNoveDigitos = Pattern.compile("[0-9]{9}").asPredicate();

    public static String formatarNoveDigitos(String noveDigitos) {
        if (!validadorNoveDigitos.test(noveDigitos)) {
            throw new NumberFormatException("String não é número com nove dígitos.");
        }

        return noveDigitos.substring(0, 1) + "."
             + noveDigitos.substring(1, 4) + "."
             + noveDigitos.substring(5, 8) + ","
             + noveDigitos.substring(8, 10);
    }

    /**
     * Só funciona se a língua do sistema operacional for PT_BR.
     */
    public static Optional<String> formatarNoveDigitosMelhor(String noveDigitos) {
        if (!validadorNoveDigitos.test(noveDigitos)) {
            return Optional.empty();
        }
        
        return Optional.of(String.format("%,.2f", Double.parseDouble(noveDigitos)));
    }
}
