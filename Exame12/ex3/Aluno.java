public class Aluno {
    public Aluno (String nome, double nota) {
        this.nome = nome;
        this.nota = nota;
    }

    public String getNome() {
        return nome;
    }

    public double getNota() {
        return nota;
    }

    private String nome;
    private double nota;
}
