import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class ControladorAlunos {
    public static void main(String... args) {
        List<Aluno> alunosCadastrados = lerAlunosCadastrados();
        List<Aluno> alunosAprovados = filtrarAlunosAprovados(alunosCadastrados, 7.0);

        for (Aluno aprovado : alunosAprovados) {
            System.out.println(aprovado.getNome());
        }

        if (alunosAprovados.size() <= 0) {
            System.out.println("Nenhum aluno foi aprovado.");
        }
    }

    private static List<Aluno> filtrarAlunosAprovados(List<Aluno> alunos, double notaMinima) {
        List<Aluno> alunosAprovados = new ArrayList<>();
        for (Aluno aluno : alunos) {
            if (aluno.getNota() >= notaMinima) {
                alunosAprovados.add(aluno);
            }
        }
        return alunosAprovados;
    }

    /**
     * Implementação com métodos anônimos.
     */
    private static List<Aluno> filtrarAlunosAprovadosMelhor(List<Aluno> alunos, double notaMinima) {
        return alunos.stream()
            .filter(aluno -> aluno.getNota() >= notaMinima)
            .collect(Collectors.toList());
    }

    private static List<Aluno> lerAlunosCadastrados() {
        Scanner teclado = new Scanner(System.in);
        List<Aluno> alunosEntrados = new ArrayList<>();

        System.out.println("Escreva os nomes e notas dos alunos. Entre uma linha em branco para encerrar.");
        String leitura = teclado.nextLine().strip();
        while (!leitura.isEmpty()) {
            String[] linhaTabela = leitura.split(";");
            alunosEntrados.add(new Aluno(linhaTabela[0], Double.parseDouble(linhaTabela[1])));
        }

        return alunosEntrados;
    }
}
