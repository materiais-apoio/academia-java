package ex6;

public interface Comparavel {
    boolean saoIguais(Object obj);
}

/**
 * Usando Generics.
 */
interface ComparavelMelhor<T> {
    boolean saoIguais(T obj);
}