package ex6;

import java.time.LocalDate;

public class Pessoa implements Comparavel {

    public boolean saoIguais(Object obj) {
        if (obj instanceof Pessoa) {
            Pessoa outra = (Pessoa) obj;
            return this.nomeCompleto.equals(outra.nomeCompleto)
                && this.cpf.equals(outra.cpf)
                && this.dataNascimento.equals(outra.dataNascimento);
        }
        
        return false;
    }

    private String nomeCompleto;
    private String cpf;
    private LocalDate dataNascimento;
}
