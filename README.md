# Academia Java -- UniFil - AtoS

Disponibilizarei neste repositório os códigos que desenvolver em sala de aula com vocês. Ao final de cada aula, farei sincronização, então bastará a você baixarem, manualmente ou com o git.

Cada diretório conterá uma atividade desenvolvida em sala, não necessariamente um por aula.